
Program prezentuje rozwiązanie poniższego zadania wykonywanego w ramch kursu programowania Java. 

1. Napisać klasę czytającą z pliku csv o strukturze: name;age;isVip 
Przykładowe dane: 
Andrzej;52;true 
Krzysio;45;true 
Mieszko;38;false 
 2. Stworzyć klasę Candidate (name, age, isVip) oraz napisać klasę Saver, która potrafi zapisać obiekty 
klasy Candidate do pliku o podanej ścieżce 
3. Napisać metody przyjmującą kolekcję obiektów Candidate i zwracające pod-kolekcje: 
  - 1 metoda zwraca kolekcję zawierającą tylko niepełnoletnich 
  - 2 metoda zwraca kolekcję zawierającą tylko dorosłych vipów 
  - 3 metoda: wszystkich za wyjątkiem "Andrzejów" 
4. Połączyć postałe kody by program czytał z pliku csv, zapisywał wczytany plik do kolekcji obiektów 
Candidate, tworzył trzy podkolekcje i każdą z nich zapisywał do pliku wyjściowego. 