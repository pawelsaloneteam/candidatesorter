import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

class Saver {

    private FileWriter fileWriter;
    private BufferedWriter bufferedWriter;

    Saver(Path path) {
        try {
            fileWriter = new FileWriter(path.toString(), true);
            bufferedWriter = new BufferedWriter(fileWriter);
        } catch (IOException e) {
            System.out.println("File to write was not opened: " + path.toString());
            try {
                bufferedWriter.close();
                fileWriter.close();
            } catch (IOException ep) {
                System.out.println("Attempt to close output file failed: " + path.toString());
            }
        }
    }

    void saveCandidates(List<Candidate> candidates) {
        candidates.forEach(
                candidate -> {
                    try {
                        bufferedWriter.write(candidate.toCsvLine());
                        bufferedWriter.newLine();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
        );
    }

    void close() throws IOException {
        bufferedWriter.close();
        fileWriter.close();
    }


}
