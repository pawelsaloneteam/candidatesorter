import java.io.IOException;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        DataReader dataReader = new DataReader(Paths.get("candidates.csv"));

        List<Candidate> candidates;

        try {
            candidates = getCandidates(dataReader);
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Loading candidates failed");
            return;
        } finally {
            try {
                dataReader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        List<Candidate> underageCandidates = getUnderageCandidates(candidates);
        List<Candidate> adultVipCandidates = getAdultVipCandidates(candidates);
        List<Candidate> noAndrzejCandidates = getNoAndrzej(candidates);

        Saver underageSaver = new Saver(Paths.get("underage.csv"));
        underageSaver.saveCandidates(underageCandidates);
        try {
            underageSaver.close();
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Saver close attempt failed");
        }
        Saver adultVipSaver = new Saver(Paths.get("adultVip.csv"));
        adultVipSaver.saveCandidates(adultVipCandidates);
        try {
            adultVipSaver.close();
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Saver close attempt failed");
        }
        Saver noAndrzejSaver = new Saver(Paths.get("noAndrzej.csv"));
        noAndrzejSaver.saveCandidates(noAndrzejCandidates);
        try {
            noAndrzejSaver.close();
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Saver close attempt failed");
        }


    }

    private static List<Candidate> getUnderageCandidates(List<Candidate> candidates) {
        List<Candidate> underageCandidates = new LinkedList<>();
        candidates.forEach((candidate) -> {
            if (candidate.getAge() < 18) {
                underageCandidates.add(candidate);
            }
        });
        return underageCandidates;
    }

    private static List<Candidate> getAdultVipCandidates(List<Candidate> candidates) {
        List<Candidate> adultVipCandidates = new LinkedList<>();
        candidates.forEach(candidate -> {
            if (candidate.getAge() >= 18 && candidate.isVip()) {
                adultVipCandidates.add(candidate);
            }
        });
        return adultVipCandidates;
    }

    private static List<Candidate> getNoAndrzej(List<Candidate> candidates) {
        List<Candidate> noAndrzejCandidates = new LinkedList<>();
        candidates.forEach(candidate -> {
            if (!candidate.getName().equals("Andrzej")) {
                noAndrzejCandidates.add(candidate);
            }
        });
        return noAndrzejCandidates;
    }

    private static List<Candidate> getCandidates(DataReader fileInput) throws IOException {
        List<Candidate> candidates = new LinkedList<>();
        String line;
        while (null != (line = fileInput.getLine())) {
            String[] lineSplited = line.split(";");
            Candidate candidate = new Candidate(lineSplited[0], Integer.parseInt(lineSplited[1]), Boolean.parseBoolean(lineSplited[2]));
            candidates.add(candidate);
        }
        return candidates;
    }

}
