public class Candidate {
    private String name;
    private int age;
    private boolean isVip;

    Candidate(String name, int age, boolean isVip) {
        this.name = name;
        this.age = age;
        this.isVip = isVip;
    }

    String getName() {
        return name;
    }

    int getAge() {
        return age;
    }

    boolean isVip() {
        return isVip;
    }

    String toCsvLine() {
        return name + "," + age + "," + isVip;

    }
}
