import java.io.*;
import java.nio.file.Path;

class DataReader {
    private BufferedReader inputBuffered;
    private FileReader input;

    DataReader(Path path) {
        try {
            File file = path.toFile();
            input = new FileReader(file);
            inputBuffered = new BufferedReader(input);
        } catch (FileNotFoundException e) {
            System.out.println("File not exist :" + path.toString());
            try {
                inputBuffered.close();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            try {
                input.close();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }

    String getLine() throws IOException {
        return inputBuffered.readLine();
    }

    void close() throws IOException {
        input.close();
        inputBuffered.close();
    }


}
